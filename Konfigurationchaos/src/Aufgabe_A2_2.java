import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class Aufgabe_A2_2 {
	public static void main(String[] args) // Hier startet das Programm 
	  { 
	     
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl1 = myScanner.nextInt();  
	    
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis = zahl1 + zahl2;  
	    int minus = zahl1 - zahl2;
	    int multipli = zahl1 * zahl2;
	    int geteilt = zahl1 / zahl2;
	    
	    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
	    
	    System.out.print("\n\n\nErgebnis der Deduction lautet: ");
	    System.out.print(zahl1 + " - " + zahl2 + " = " + minus);
	    
	    System.out.print("\n\n\nErgebniss der Multiopication lautet: ");
	    System.out.print(zahl1 + " * " + zahl2 + " = " + multipli);
	    
	    System.out.print("\n\n\nErgibniss der Dividation lautet ");
	    System.out.print(zahl1 + " � " + zahl2 + " = " + geteilt);
	 
	    myScanner.close(); 
	  }
}
