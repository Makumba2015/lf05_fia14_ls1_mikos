package Kuba;

public class Aufgabe_2_Neu {
	public static void main(String[] args) {
		
		System.out.printf("%-5s", "0!");
		System.out.printf("=" );
		System.out.printf("%-19s", "");
		System.out.printf("=");
		System.out.printf("%4s", "1");
		
		System.out.printf("\n%-5s", "1!");
		System.out.printf("=" );
		System.out.printf("%2s", "1");
		System.out.printf("%-17s", "");
		System.out.printf("=");
		System.out.printf("%4s", "1");
		
		System.out.printf("\n%-5s", "2!");
		System.out.printf("=" );
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%-13s", "");
		System.out.printf("=" );
		System.out.printf("%4s", "2");
		
		System.out.printf("\n%-5s", "3!");
		System.out.printf("=" );
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%-9s", "");
		System.out.printf("=" );
		System.out.printf("%4s", "6");
		
		System.out.printf("\n%-5s", "4!");
		System.out.printf("=" );
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "4");
		System.out.printf("%-5s", "");
		System.out.printf("=" );
		System.out.printf("%4s", "24");
		
		System.out.printf("\n%-5s", "5!");
		System.out.printf("=" );
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "4");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "5");
		System.out.printf("%-1s", "");
		System.out.printf("=" );
		System.out.printf("%4s", "120");
	}

}